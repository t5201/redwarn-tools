<?php
// rpm.php - (c) RedWarn Contributors - Apache License 2.0
// Core by Ed. E
// Publicly returns the result from rpm.php in cronTasks in json format
header("Content-Type: application/json; charset=UTF-8"); // set json response
$jsonArr = []; // array to be converted to JS
$lines = explode("\n", file_get_contents("../../cronTools/rpmData.txt"));
foreach ($lines as $line) {
    $lineData = explode("|", $line);
    if ($lineData[0] == "") continue; // If line is empty, skip
    $jsonArr[] = array(
        "timestamp" => $lineData[0],
        "undoRPM" => $lineData[1],
        "rollbackRPM" => $lineData[2],
        "totalRPM" => $lineData[3]
    );
}
unset($line); // clear just in case because PHP is stinky and doesn't do this for us

// Now return JSON
echo json_encode($jsonArr);