// Oauth things
var passport = require( "passport" );
var MediaWikiStrategy = require( "passport-mediawiki-oauth" ).OAuthStrategy;
var config = require( "./config" );
var randomToken = require('random-token');

// IRC
var irc = require('irc'); 

var authedUsers = {}; // only users in this object can connect via a private token returned by - if lost can be reset on reauth
var assosiatedSockets = {}; // "Username" : [] of sockets so that it can send
var sessions = {};

// Authentication

var express = require( "express" );
var session = require( "express-session" );
var passport = require( "passport" );
var MediaWikiStrategy = require( "passport-mediawiki-oauth" ).OAuthStrategy;
var config = require( "./config" );
const e = require("express");

var app = express();
var expressWs = require('./express-ws')(app);
var router = express.Router();

app.set( "views", __dirname + "/public/views" );
app.set( "view engine", "ejs" );
app.use( express.static(__dirname + "/public/views") );

app.use( passport.initialize() );
app.use( passport.session() );

app.use( session({ secret: "OAuth Session",
	saveUninitialized: true,
	resave: true
}) );

app.use( "/", router );

passport.use(
	new MediaWikiStrategy({
		consumerKey: config.consumer_key,
		consumerSecret: config.consumer_secret
	},
	function ( token, tokenSecret, profile, done ) {
		profile.oauth = {
			consumer_key: config.consumer_key,
			consumer_secret: config.consumer_secret,
			token: token,
			token_secret: tokenSecret
		};
		return done( null, profile );
	}
	) );

passport.serializeUser(	function ( user, done ) {
	done( null, user );
});

passport.deserializeUser( function ( obj, done ) {
	done( null, obj );
});

router.get( "/login", function ( req, res ) {
	res.redirect( req.baseUrl + "/oauth" );
} );
 
router.get( "/oauth", function( req, res, next ) {
	passport.authenticate( "mediawiki", function( err, user ) {
		if ( err ) {
			return next( err );
		}

		if ( !user ) {
			return res.redirect( req.baseUrl + "/login" );
		}

		req.logIn( user, function( err ) {
			if ( err ) {
				return next( err );
			}
      console.log("New OAuth login: "+ user.displayName)
      if (assosiatedSockets[user.displayName] != null) {
        // Now loop for each and tell sockets - again completely insecure - todo tighten
        // let's generate a random token and add as authed
        if (authedUsers[user.displayName] == null) authedUsers[user.displayName] = []; // set up array if missing
        let token = randomToken(16); // generate token
        // Add to authed user array
        authedUsers[user.displayName].push(token);

        assosiatedSockets[user.displayName].forEach(ws=>{
          try {
            ws.send("TOKEN|"+ token);
          } catch (error) {
            // ignore
          }
        });
      }
			res.render( "index" );
		} );
	} )( req, res, next );
} );




// End auth
// Main flow
router.ws('/gateway', function(ws, req) {
  console.log("new socket");
  // Set linked user
  let linkedUser = req.query["user"];
  if (linkedUser == null) {
    ws.send("You must connect with /gateway?user=globalaccountname");
    ws.close();
    return;
  } else {
    // Note: ofc, this isn't at all secure hijacking wise, but there is no point as IRC spoofing is relatively easy anyways - but ALL tokens that could comprimise an actual
    // WMG account SHOULD NEVER EVER EVER be sent here

    console.log("Assosiated with "+ linkedUser);

    // Establish our var if it isn't there
    if (assosiatedSockets[linkedUser] == null) assosiatedSockets[linkedUser] = [];

    // Add our socket to assosiated sockets for token
    assosiatedSockets[linkedUser].push(ws);

    // Remove ours on close
    ws.on('close', ()=>assosiatedSockets[linkedUser].splice(assosiatedSockets[linkedUser].indexOf(ws), 1));
  }
  
  // Wait for connection
  let WSonmessage = function incoming(message) {
  let values = message.split("|");
  if ((values.length < 3) || (authedUsers[linkedUser] == null) || (!authedUsers[linkedUser].includes(values[1]))) {
    // Malformed request or bad token
    ws.send("BADREQ");
  } else {
    // Rm message handler
    ws.off('message', WSonmessage);
    // We can continue
    let tool = values[2];
    let nickname = linkedUser;
    
    ws.send(`Connecting...
    Nickname: ${nickname}
    Tool: ${tool}`);

    

    // Establish IRC connection
    if (sessions[nickname] == null) { // If session doesn't exist
      sessions[nickname] = {"activeConnections":0}; // establish
      sessions[nickname].client = new irc.Client('irc.tm-irc.org', nickname, { // create new IRC session 
          channels: ['#en.wikipedia.huggle'],
          userName : nickname,
          realName : `${tool} via HANgateway`,
          autoRejoin: true,
          port: 6667
      });
      sessions[nickname].client.addListener('registered', ()=>{
        ws.send("CONNECTED"); // send connected signal
      });
    } else {
      // Already exists - clear timeout
      clearTimeout(sessions[nickname].timer);
      ws.send("CONNECTED"); // send connected signal
    }

    sessions[nickname].activeConnections += 1; // add to active connections

    // Now add handlers
    let rawListener = function (message) {
      if (message.command == "PRIVMSG") { // on normal message
          ws.send(JSON.stringify({
            "type" : "message",
            "from" : message.nick,
            "content" : message.args[1]
          }));
      }
    };
    sessions[nickname].client.addListener('raw', rawListener);

    let errorListener = function(message) { // on error
      ws.send(JSON.stringify({
        "type" : "error",
        "info" : message
      }));
    };
    sessions[nickname].client.addListener('error', errorListener);

    let namesListener = function(channel, nicks) { // on user list
      ws.send(JSON.stringify({
        "type" : "online",
        "info" : nicks
      }));
    };
    sessions[nickname].client.addListener('names', namesListener);


    // On me sending a message, respond as usual so it gets logged
    let selfMsgListener = (to, text) =>{
      ws.send(JSON.stringify({
        "type" : "message",
        "from" : nickname,
        "content" : text
      }));
    };
    sessions[nickname].client.addListener('selfMessage', selfMsgListener);

    // On session message
    ws.on('message', function incoming(message) {
      if (message == "GETNAMES") {
        // Get usernames request
        sessions[nickname].client.send("NAMES", "#en.wikipedia.huggle"); // request usernames
      } else {
        sessions[nickname].client.say('#en.wikipedia.huggle', message); // send normal message
      }
    });

    // On disconnect
    ws.on('close', ()=>{
      // We just need to clear all the listeners
      sessions[nickname].client.removeListener("raw", rawListener);
      sessions[nickname].client.removeListener("error", errorListener);
      sessions[nickname].client.removeListener("names", namesListener);
      sessions[nickname].client.removeListener('selfMessage', selfMsgListener);

      // Remove our session from the number of connections
      sessions[nickname].activeConnections -= 1; 

      if (sessions[nickname].activeConnections == 0) {
        // If no active connections, set IRC disconnect timer going
        // Now we wait 20 seconds and then wipe and disconnect
        sessions[nickname].timer = setTimeout(()=>{
          // Wipe and disconnect
          sessions[nickname].client.disconnect("HANgateway: all sockets disconnected 20s ago - goodbye!");
          sessions[nickname] = null;
        }, 20000);
      }  
      });
    }
  };

  // Add on message
  ws.on('message', WSonmessage);

  // Connection welcome message
  ws.send(`
Welcome to the RedWarn tools Huggle anti-vandal network gateway - powered by Wikimedia Toolforge.
You must authenticate with a Wikimedia global account to use this service. To do so, navigate to https://hangateway.toolforge.org/login.
On OAUTH completion, you will be returned a token.

To connect to the English Wikipedia HAN, type "connect|(your token)|(your toolname)" - you will then be provided with a connection.
If you become disconnected, the server will retain your IRC connection for up to 20 seconds.
`);

});

// Finally, serve
app.listen( process.env.PORT || 5000, function () {
	console.log( "Node.js app listening" );
} );